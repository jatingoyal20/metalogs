
#!/usr/bin/env python
import json
import requests
import common
import datetime

class groupByCohort:
    def __init__(self,groupby,filters,startdate,enddate,url,tables):
        self.tables=tables
        self.query=dict()
        bound = {"type": "bound", "dimension": "install_date", "lower": startdate.split('T')[0],
                 "upper": enddate.split('T')[0], "alphaNumeric": "true"}
        self.query["filter"]=dict()
        filters = common.getFilter(filters)
        if (filters):
            self.query["filter"] = filters
            self.query["filter"]["fields"].append(bound)
        else:
            self.query["filter"] = bound

        self.groupby=common.getDimensionDisplayName(groupby)


        self.url=url
        self.query["queryType"] = "groupBy"
        self.query["granularity"] = "all"
        self.query["intervals"] =  startdate + "/" + datetime.datetime.now().isoformat().split('T')[0]
        self.query["dimensions"] = ["days_since_install", common.getDimensionLookup(groupby)]






    def queryDruidGraph(self,datasource,metrics):
        self.query["dataSource"] = datasource
        self.query["aggregations"] = []
        self.query["metric"] = metrics[0] #default metric to sort on

        for metricName in metrics:
            self.query["aggregations"].append(common.getMetricLookup(metricName))

        print json.dumps(self.query)
        r = requests.post(self.url, data=json.dumps(self.query))
        return r.json()


    def batchQuerying(self,metrics,xdimension):
        final=dict()
        ffinal = dict()

        maps=dict()
        for metricName in metrics:
            final[common.getMetricDisplayName(metricName)] = dict()
            if (common.getMetricDataSource(metricName) not in maps):
                maps[common.getMetricDataSource(metricName)] = []
            maps[common.getMetricDataSource(metricName)].append(metricName)
            for table in self.tables:
                final[common.getMetricDisplayName(metricName)][table] = []


        for datasource, metricd in maps.iteritems():
            queries=self.queryDruidGraph(datasource,metricd)
            final[xdimension] = []

            if queries:
                days=dict()
                for query in queries:
                    dayno=int(query["event"][xdimension])
                    if dayno not in days:
                        days[dayno]=[]
                    days[dayno].append(query["event"])

                for key,value in days.iteritems():
                    if(key<0):
                        continue
                    for metricName in metricd:
                        for table in self.tables:
                            display=common.getMetricDisplayName(metricName)
                            final[display][table].append(0)
                    final[xdimension].append(key)


                    for element in value:
                        for metricName in metricd:
                            display=common.getMetricDisplayName(metricName)
                            id=element[self.groupby]
                            final[display][id][-1]=element[display]

        ffinal[xdimension] = final[xdimension]
        for metric in metrics:
            ffinal[metric + ',' + common.getMetricDisplayName(metric)] = final[common.getMetricDisplayName(metric)]

        # print metrics
        print ffinal
        return ffinal