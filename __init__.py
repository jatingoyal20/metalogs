#!/usr/bin/env python
from flask import Flask, request, render_template, session, url_for, redirect, make_response
from tabular import *
from cumulateStandard import *
from groupByCohort import *
from cumulateCohort import *
from groupByStandard import *
from collections import defaultdict
import common

# create my flask application
app = Flask(__name__)
# setting the secret key
app.secret_key = 'rohitjoseph0808@gmail.com'

url='http://10.5.1.8:8082/druid/v2/'
#r = requests.get(url+"datasources/apps_searchprivacy/dimensions")
#dimensions=r.json()
#dimensions.sort()
#dimensions =dimensions[:2]
#print dimensions
#metrics=["Active_Users","Installs","apps_revenue","Uninstalls","Searches","Ad_Click"]



@app.route("/<consumerGroup>", methods=['GET', 'POST'])
def login(consumerGroup):#login page
    if common.getConsumerGroupExists(consumerGroup):
        odimensions=common.getConsumerGroupDimensions(consumerGroup)
        ometrics=common.getConsumerGroupMetrics(consumerGroup)

        dimensions = []
        for val in odimensions:
            if val=="hour" or val=="day" or val=="none":
                continue
            dimensions.append((val,common.getDimensionDisplayName(val)))

        metrics=[]
        for val in ometrics:
            if val["isDefault"] == True:
                metrics.append((val["name"],common.getMetricDisplayName(val["name"])))

        return render_template("register.html",dimensions=dimensions,metrics=metrics)
    else:
        return render_template("400.html")

@app.route("/<consumerGroup>/dynamic", methods=['GET', 'POST'])
def graph(consumerGroup):
    metrics = []
    if common.getConsumerGroupExists(consumerGroup):
        ometrics = common.getConsumerGroupMetrics(consumerGroup)
        for val in ometrics:
            if val["isDefault"] == True:
                metrics.append(val["name"])
    else:
        return render_template("400.html")


    startdate = "2016-10-01T00:00"
    enddate = "2016-10-01T23:59"
    compareon = "cumulative"
    metrictype = "count"
    graphtype = "standard"
    plotby = "hour"

    filters = defaultdict()
    params = request.args.items()
    for cur in params:
        field = cur[0]
        if (field == "specs"):
            now = cur[1].split(',')
            if(len(now)==6):
                startdate = now[0]
                enddate = now[1]
                compareon = now[2]
                metrictype = now[3]
                graphtype = now[4]
                plotby = now[5]
            else:
                render_template("400.html")
        else:
            valueList = cur[1].split(',')

            filters[field] = []
            for value in valueList:
                if (value == "null"):
                    value = ""
                filters[field].append(value)

    tables=[]
    if compareon != "cumulative":
        htmlcheck=Tabular(5,filters, startdate, enddate, url).queryDruidTable(metrictype,compareon)
        if htmlcheck:
            htmlcheck=htmlcheck[0]
            filters[compareon]=[]
            for cur in htmlcheck["result"]:
                now=cur[common.getDimensionDisplayName(compareon)]
                tables.append(now)
                if now is not None:
                    if common.getDimensionIsExtraction(compareon):
                        now = now.split('[')
                        if (len(now)) != 1:
                            now=now[1][:-1]
                    filters[compareon].append(now)
                else:
                    filters[compareon].append("")
    print tables
    html = dict()

    print metrics
    if graphtype=="standard":
        print "Standard:"
        if (compareon == "cumulative"):
            html["standard"] = cumulateGraph(filters, startdate, enddate, url, plotby).batchQuerying(metrics)
        else:
            html["standard"] = groupByGraph(compareon, filters, startdate, enddate, url,plotby,tables).batchQuerying(metrics)
    elif graphtype=="cohort" and consumerGroup =="Apps":
        print "cohort:"
        if (compareon == "cumulative"):
            html["cohort"] = cumulateCohort(filters, startdate, enddate, url).batchQuerying(metrics,"days_since_install")
        else:
            html["cohort"] = groupByCohort(compareon, filters, startdate, enddate, url,tables).batchQuerying(metrics,"days_since_install")
    else:
        render_template("400.html")

    print json.dumps(html)
    return json.dumps(html)

@app.route("/tabular/<dimension>", methods=['GET', 'POST'])
def handletabular(dimension):
    dimension=dimension.split(',')
    startdate = "2016-10-01T00:00"
    enddate = "2016-10-01T23:59"
    compareon= "cumulative"
    metrictype="count"
    graphtype ="standard"
    plotby= "hour"

    filters=defaultdict()
    params = request.args.items()
    for cur in params:
        field=cur[0]
        if(field=="specs"):
            now=cur[1].split(',')
            if(len(now)==6):
                startdate = now[0]
                enddate = now[1]
                compareon = now[2]
                metrictype = now[3]
                graphtype = now[4]
                plotby = now[5]
            else:
                render_template("400.html")
        else:
            valueList=cur[1].split(',')

            #for regex
            if (len(dimension) != 1) and dimension[0]==field:
                continue
            filters[field]=[]
            for value in valueList:
                if (value == "null"):
                    value = ""
                filters[field].append(value)
    type = Tabular(10, filters, startdate, enddate, url)
    if(len(dimension) ==1):
        return json.dumps(type.queryDruidTable(metrictype,dimension[0]))
    else:
        type.addregex(dimension[0],dimension[1])
        return json.dumps(type.queryDruidTable(metrictype,dimension[0]))


if __name__ == '__main__':
    app.run(threaded=True,host='172.16.164.120', port=5000, debug=True, use_reloader=True)