
    function updateGraph(getURL){
        //ajax for graph
        $.ajax({
            url: document.URL.split('?')[0] + "/dynamic?" + getURL.split('?')[1],
            success: function (html) {
                console.log(html)

                data = JSON.parse(html)
                console.log(data)
                
                if("standard" in  data){
                    for (var index in data["standard"]["timestamp"]) {
                        value = data["standard"]["timestamp"][index]
                        value = value.split("-")[2]
                        value = value.split(":")[0]
                        data["standard"]["timestamp"][index] = value
                    }
                    generateBatchGraph("standard", "timestamp", data["standard"])
                }
                else {
                    generateBatchGraph("cohort", "days_since_install", data["cohort"])
                }
            }
        });
    }


    function generateBatchGraph(type, xlabel, graphData) {

            for (var key in graphData) {
            //    console.log("HI" + key)
                now=key.split(',')
                if (now.length==1) continue;
                generateIndividualGraph(now[0],now[1], type, graphData[xlabel], graphData[key])
          //   console.log("Done")
            }
        }
    
    //key,type=standard/cohort,xaxis array, yaxis array
    function generateIndividualGraph(xlabel,displayxlabel, type, xaxis, cumulate) {
        //console.log(cumulate)

        classname = '.graph'  + '-' + xlabel
        console.log(classname)
        $(classname).highcharts({
            title: {
                text: displayxlabel,
                x: -20 //center
            },
            xAxis: {
                categories: xaxis
            },
            yAxis: {
                title: {
                    text: "count"
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            }
        });

        for (var key in cumulate){
            var chart = $(classname).highcharts();
            var yaxis=cumulate[key]
            var seriesData = [];
            for (var j = 0; j < yaxis.length; j++) {
                var value = yaxis[j];
                if (value == null || isNaN(value) || value == "NaN" || value == "None") {
                    seriesData.push(0);
                } else {
                    if (xlabel == "apps_gross_revenue") value = value * 0.76
                    seriesData.push(value);
                }
            }
            chart.addSeries({
                name : key,
                data: seriesData
            });
        }
    }

