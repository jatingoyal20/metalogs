
$(document).ready(function () {

    //initialization
    var curUrl = document.URL
    var myset = {}
    var url = ""

    if (curUrl.split('?').length == 1) {
        var url = curUrl.split('?')[0]
    }
    else {
        var url = curUrl.split('?')[0]
        var getParams = curUrl.split('?')[1]
        var filters = getParams.split('&')

        for (var index in filters) {
            if (index == "") continue;
            var field = filters[index].split('=')[0]
            if (field == "specs") {

                var specs=filters[index].split('=')[1]
                specs=specs.split(',')

                $("#date-sd").val(specs[0].replace('T',' '))
                $("#date-ed").val(specs[1].replace('T',' '))
                $("#drop-down-co").val(specs[2])
                $("#drop-down-mt").val(specs[3])
                $("#drop-down-gt").val(specs[4])
                $("#drop-down-pb").val(specs[5])
            }
            else {
                var values = (filters[index].split('=')[1]).split(',')
                myset[field] = {}
                for (var valueInd in values) {
                    value = values[valueInd]
                    value=decodeURIComponent(value)
                    myset[field][value] = $("#"+field+" u").text()+":"+value;
                }
            }
        }
    }

    $("#query--submit").click(function (event) {
                    updateUrl();
                    updateGraph(getUrl());
                    updateTabular();
    })
    
    $("#drop-down-co").on('change',function (event) {
                    updateUrl();
                    updateGraph(getUrl());
    })
    $("#drop-down-mt").on('change',function (event) {
                    updateUrl();
                    updateTabular();
    })
    $("#drop-down-gt").on('change',function (event) {
                    updateUrl();
                    updateGraph(getUrl());
    })
    $("#drop-down-pb").on('change',function (event) {
                    updateUrl();
                    updateGraph(getUrl());
    })
    
    


    $(".searchinput").on('keyup',function (event) {
            childclass=$(this).parents(".tabdv").attr('id')
            pattern=$(this).val()
            console.log(childclass,pattern)
            if(pattern=="") bakcajax(childclass,getUrl(),childclass)
            else bakcajax(childclass,getUrl(),childclass+','+encodeURIComponent(pattern))
    });

    $('.search').on('click', function(e){
        e.stopPropagation();
        $(this).parents('u').siblings('.srbx').fadeToggle();
    });

    updateUrl();
    updateGraph(getUrl());
    updateTabular();

    //  var now='{{graphData}}'
    //  now=now.replace(/&#34;/gi,"\"")


    function removeFilter(key, value) {
        delete myset[key][value]
        if (Object.keys(myset[key]).length == 0) {
            delete myset[key]
        }
    }
    function addFilter(key, value) {
        display=$("#"+key+" u").text()+":"+value;
        
        cur = value.split('[')
        if (cur.length == 1) {
            value = cur[0]
        }
        else {
            value = cur[1].substr(0, cur[1].length - 1)
        }
       
        // console.log(value)
        if (myset[key]) {
            myset[key][value] = display
        }
        else {
            myset[key] = {}
            myset[key][value] = display
        }
    }
    function getUrl() {
        var redirectUrl = url + "?"
        var start_date = $('#date-sd').val().replace(' ','T');
        var end_date = $('#date-ed').val().replace(' ','T');
        var compare_on = $("#drop-down-co").val();
        var metric_type = $("#drop-down-mt").val();
        var graph_type=$("#drop-down-gt").val();
        var plot_by=$("#drop-down-pb").val();
        redirectUrl += "specs=" + start_date + "," + end_date + "," + compare_on + "," + metric_type + "," + graph_type+ "," + plot_by;

        for (var key in myset) {
            var now = "&" + key + "="
            for (var value in myset[key]) {
                encoded=encodeURIComponent(value);
                now += encoded + ","
            }
            now = now.substr(0, now.length - 1)
            redirectUrl += now
        }
        return redirectUrl
    }
    function bakcajax(childclass,curUrl,pattern){
            $("#"+childclass).find(".entries").html("Loading.....")
            $.ajax({
                url: document.URL.split('/')[0] + "/tabular/"+pattern+"?"+ curUrl.split('?')[1],
                success: function (html) {
                    //console.log(html)
                    var data = JSON.parse(html)
                    var result=[];
                    if (data.length >0) {
                        data=data[0]
                        result=data["result"]
                    }
                    //console.log(result)
                    

                    out = ""
                    for (var inds in result) {
                            value=result[inds]

                            //apps_team check
                            if(childclass=="apps_gross_revenue") value[data["metric"]] =0.76*value[data["metric"]];

                            value[data["metric"]]=parseInt(value[data["metric"]])
                            out = out + '<div id="' + childclass + ',' + value[data["dimension"]] + '"class="addClass">' + '<span class="value">'+value[data["dimension"]] + '</span><span class="count">' + value[data["metric"]] + '</span></div>'
                    }
                    $("#"+childclass).find(".entries").html(out)   
                    
        
                    $('#'+childclass).find('.addClass').click(function (event) {
                       
                        fields = $(this).attr('id').split(',');
                        addFilter(fields[0], fields[1])
                        //console.log(fields)
                        updateUrl();
                        updateGraph(getUrl());
                        updateTabular();
                    })
                }
            });
        }

    

    
    
    function updateUrl(){
            history.pushState("", "", getUrl())
    }
    function updateTabular() {


        var out = "<span>Filters</span>";
        for (var key in myset) {
            for (var value in myset[key]) {
                out = out + '<div id="' + key + ',' + value + '"class="removeClass">' + myset[key][value] + '<span class="X">X</span></div>'
            }
        }
        $(".filters").html(out)

        //console.log(getUrl())
       $(".removeClass").find('.X').click(function (event) {
                event.stopPropagation();
                fields = $(this).parents('.removeClass').attr('id').split(',');
                removeFilter(fields[0], fields[1])
                //  window.location =getUrl()
                updateUrl();
                updateGraph(getUrl());
                updateTabular();
            });  
    
        
        //ajax separate
        var childrens=$('.tabular').children()
        for (var index=0 ;index <childrens.length;index++){
                var childId=$(childrens[index]).attr('id')  
                bakcajax(childId,getUrl(),childId)
        }  
        
        

        
    }
})