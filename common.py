import json


dimension=dict()
metric=dict()
consumerGroup=dict()

with open('config/dimensions.json') as json_data:
    dimensionList = json.load(json_data)
    for value in dimensionList:
        dimension[value["myName"]]=value

with open('config/metrics.json') as json_data:
    metricsList = json.load(json_data)
    for value in metricsList:
        metric[value["myName"]]=value

with open('config/groups.json') as json_data:
    consumerGroup = json.load(json_data)

def getConsumerGroupExists(consumerName):
    if consumerName in consumerGroup:
        return True
    else:
        return False

def getConsumerGroupMetrics(consumerName):
    return consumerGroup[consumerName]["metrics"]

def getConsumerGroupDimensions(consumerName):
    return consumerGroup[consumerName]["dimensions"]

def getConsumerGroupDisplayName(consumerName):
    return consumerGroup[consumerName]["displayName"]

def getDimensionDruidName(dimensionName):
    return dimension[dimensionName]["dimension"]["dimension"]

def getDimensionDisplayName(dimensionName):
    return dimension[dimensionName]["dimension"]["outputName"]

def getDimensionIsExtraction(dimensionName):
    if dimension[dimensionName]["dimension"]["type"] == "extraction":
        return True
    else:
        return False

def getDimensionLookup(fieldName):
    return dimension[fieldName]["dimension"]

def getMetricDataSource(metricName):
    return metric[metricName]["dataSource"]

def getMetricDisplayName(metricName):

    return metric[metricName]["displayName"]

def getMetricLookup(metricName):
    return metric[metricName]["aggregation"]


# returns  {} if empty, else {"type":and,"fields"[]}
def getFilter(filters):
    lists=[]
    for key in filters:
        ors=dict()
        ors["type"]="or"
        ors["fields"]=[]
        for value in filters[key]:
            selector=dict()
            selector["type"] = "selector"
            selector["dimension"] =getDimensionDruidName(key)
            selector["value"]=value
            lookup=getDimensionLookup(key)
            if "name" in lookup:
                selector["extractionFn"]={"type":"registeredLookup","lookup":lookup["name"],"replaceMissingValueWith":"Unknown"}
            ors["fields"].append(selector)
        lists.append(ors)

    if not lists:
        return {}
    else:
        ands = dict()
        ands["type"]="and"
        ands["fields"]=lists
        return ands

