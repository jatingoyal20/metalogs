#!/usr/bin/env python
import datetime
import mappings
import requests

class Cohort:
    def __init__(self,limit,filters,startdate,enddate,url):
        self.limit=limit
        self.filters=filters

        bound={
                    "type": "bound",
                    "dimension": "install_date",
                    "lower": startdate.split('T')[0],
                    "upper": enddate.split('T')[0] ,
                    "alphaNumeric": "true"
                }

        if(self.filters):
            self.filters["fields"].append(bound)
        else:
            self.filters=bound
        self.startdate=startdate
        currentDate = datetime.datetime.now().isoformat().split('T')[0]
        self.enddate=currentDate
        self.url=url

    def getAggregation(self,metricName):
        for value in mappings.mapAggregations:
            if(value["myName"]==metricName):
                return value["aggregation"]
        return  { "fieldName": "event_value","name": "Event Value","type": "longSum"}

    def getDataSource(self, metricName):
        if (metricName == "ad_click_gross_revenue"):
            return "apps_stats_audit"
        else:
            return "apps_searchprivacy"

    def queryDruidCohort(self,metricName):

        query=dict()
        query["queryType"] = "topN"
        query["dataSource"] = self.getDataSource(metricName)
        query["intervals"]=self.startdate+"/"+self.enddate
        query["granularity"] = "all"
        if self.filters:
            query["filter"]=self.filters
        query["dimension"] = "days_since_install"
        query["metric"]=metricName
        query["threshold"]=self.limit

        agg = self.getAggregation(metricName)
        if agg["type"] == "filtered":
            agg["aggregator"]["name"] = metricName
        else:
            agg["name"] = metricName

        query["aggregations"] = [agg]
        print json.dumps(query)

        r = requests.post(self.url, data=json.dumps(query))
        return r.json()

    def batchQuerying(self, metrics):
        cur = dict()
        query = self.queryDruidCohort(metrics[0])


        # numbers
        numbers = []
        for values in query[0]["result"]:
            numbers.append(int(values["days_since_install"]))
        numbers.sort()
        cur["days_since_install"]=numbers


        # metric
        for metric in metrics:
            query = self.queryDruidCohort(metric)
            cur[metric] = []
            now=dict()
            for values in query[0]["result"]:
                now[int(values["days_since_install"])]=values[metric]
            for values in numbers:
                cur[metric].append(now[values])
        #print json.dumps(cur)
        return cur