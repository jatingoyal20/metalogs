
#!/usr/bin/env python
import json
import requests
import datetime
import common

class cumulateCohort:
    def __init__(self,filters,startdate,enddate,url):

        self.url=url
        self.query=dict()
        self.query["queryType"] = "topN"
        self.query["granularity"] = "all"
        self.query["intervals"] = startdate + "/" + datetime.datetime.now().isoformat().split('T')[0]

        bound={"type": "bound","dimension": "install_date","lower": startdate.split('T')[0],"upper": enddate.split('T')[0] ,"alphaNumeric": "true"}
        filters = common.getFilter(filters)
        if (filters):
            self.query["filter"] = filters
            self.query["filter"]["fields"].append(bound)
        else:
            self.query["filter"] = bound

        self.query["dimension"] = "days_since_install"
        self.query["descending"] = "false"
        self.query["threshold"]=1000


    def queryDruidGraph(self,datasource,metrics):
        self.query["dataSource"] = datasource
        self.query["aggregations"] = []
        self.query["metric"] = common.getMetricDisplayName(metrics[0]) #deafult metric to sort on

        for metricName in metrics:
            self.query["aggregations"].append(common.getMetricLookup(metricName))

        print json.dumps(self.query)
        r = requests.post(self.url, data=json.dumps(self.query))
        return r.json()


    def batchQuerying(self,metrics,xdimension):
        maps=dict()
        final=dict()
        ffinal=dict()

        for metric in metrics:
            final[common.getMetricDisplayName(metric)] = dict()
            final[common.getMetricDisplayName(metric)]["cumulative"] =[]
            if(common.getMetricDataSource(metric) not in maps):
                maps[common.getMetricDataSource(metric)]=[]
            maps[common.getMetricDataSource(metric)].append(metric)


        for datasource, metricd in maps.iteritems():
            query=self.queryDruidGraph(datasource,metricd)
            final[xdimension] = []
            if query and ("error" not in query):
                query[0]["result"]=sorted(query[0]["result"],key=lambda key: int(key[xdimension]))
                #print query[0]["result"]
                for entity in query[0]["result"]:
                    if(int(entity[xdimension])<0):
                        continue
                    for metricName,value in entity.iteritems():
                        if(metricName==xdimension):
                            final[metricName].append(value)
                        else:
                            final[metricName]["cumulative"].append(value)

        ffinal[xdimension] = final[xdimension]
        for metric in metrics:
            ffinal[metric + ',' + common.getMetricDisplayName(metric)] = final[common.getMetricDisplayName(metric)]

        # print metrics
        return ffinal
