false=False
y=[

  {
    "aggregation": {
      "fieldName": "count",
      "name": "Count",
      "type": "longSum"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Count",
    "isDefault": false,
    "myName": "apps_searchprivacycount"
  },
  {
    "aggregation": {
      "fieldName": "DistinctUsers",
      "name": "Unique Users",
      "type": "hyperUnique"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Unique Users",
    "isDefault": false,
    "myName": "apps_distinctusers"
  },
  {
    "aggregation": {
      "fieldName": "event_value",
      "name": "Event Value",
      "type": "longSum"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Event Value",
    "isDefault": false,
    "myName": "apps_eventvalue"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Landing Impressions",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "LandingImpression"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Landing Impressions",
    "isDefault": false,
    "myName": "apps_landingimpressions"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Installs",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "InstallSuccess"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Installs",
    "isDefault": false,
    "myName": "apps_uniqueinstall"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Searches",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "searchcount"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Searches",
    "isDefault": false,
    "myName": "apps_searches"
  },{
  "aggregation": {
    "aggregator": {
      "fieldName": "event_value",
      "name": "Searches Graph",
      "type": "longSum"
    },
    "filter": {
      "fields" :[
        {
          "dimension": "event",
          "type": "selector",
          "value": "searchcount"
        }
      ],
      "type" : "and"
    },
    "type": "filtered"
  },
  "dataSource": "apps_searchprivacy",
  "displayName": "Searches Graph",
  "isDefault": false,
  "myName": "apps_searchesgraph"
},
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Ad Click",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "adclickcount"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Ad Click",
    "isDefault": false,
    "myName": "apps_adclickcount"
  },{
  "aggregation": {
    "aggregator": {
      "fieldName": "event_value",
      "name": "Ad Click Graph",
      "type": "longSum"
    },
    "filter": {
      "fields" :[
        {
          "dimension": "event",
          "type": "selector",
          "value": "adclickcount"
        }
      ],
      "type" : "and"
    },
    "type": "filtered"
  },
  "dataSource": "apps_searchprivacy",
  "displayName": "Ad Click Graph",
  "isDefault": false,
  "myName": "apps_adclickcountgraph"
},
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Uninstalls",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "Uninstall"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Uninstalls",
    "isDefault": false,
    "myName": "apps_unistalls"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Skip Offer",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "SkipOffer"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Skip Offer",
    "isDefault": false,
    "myName": "apps_skipoffer"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Install Click",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "InstallClick"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Install Click",
    "isDefault": false,
    "myName": "apps_installclick"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Install Cancel",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "InstallCancel"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Install Cancel",
    "isDefault": false,
    "myName": "apps_installcancel"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Already Installed",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "AlreadyInstalled"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Already Installed",
    "isDefault": false,
    "myName": "apps_alreadyinstalled"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Click Tab Close",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "ClickTabClose"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Click Tab Close",
    "isDefault": false,
    "myName": "apps_clicktabclose"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Install Failed",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "InstallFailed"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Install Failed",
    "isDefault": false,
    "myName": "apps_installfailed"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "Churn User",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "ChurnUser"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Churn User",
    "isDefault": false,
    "myName": "apps_churnuser"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "TosClick",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "TosClick"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "TosClick",
    "isDefault": false,
    "myName": "apps_tosclick"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "PpClick",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "PpClick"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "PpClick",
    "isDefault": false,
    "myName": "apps_ppclick"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "errorevent",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "errorevent"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "errorevent",
    "isDefault": false,
    "myName": "apps_errorevent"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "event_value",
        "name": "toggle",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "toggle"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "toggle",
    "isDefault": false,
    "myName": "apps_toggle"
  },
  {
    "aggregation": {
      "aggregator": {
        "type": "hyperUnique",
        "name": "Active Users",
        "fieldName": "DistinctUsers"
      },
      "filter": {
        "type": "and",
        "fields": [{
          "type": "selector",
          "dimension": "event",
          "value": "currentstatus"
        },
          {
            "type": "selector",
            "dimension": "strvalue",
            "value": "enabled"
          }]
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Active Users",
    "isDefault": false,
    "myName": "apps_activeusers"
  },{
  "aggregation": {
    "aggregator": {
      "type": "hyperUnique",
      "name": "Active Users Graph",
      "fieldName": "DistinctUsers"
    },
    "filter": {
      "type": "and",
      "fields": [{
        "type": "selector",
        "dimension": "event",
        "value": "currentstatus"
      },
        {
          "type": "selector",
          "dimension": "strvalue",
          "value": "enabled"
        }]
    },
    "type": "filtered"
  },
  "dataSource": "apps_searchprivacy",
  "displayName": "Active Users",
  "isDefault": false,
  "myName": "apps_activeusersgraph"
},
  {
    "aggregation": {
      "aggregator": {
        "type": "hyperUnique",
        "name": "Disabled Users",
        "fieldName": "DistinctUsers"
      },
      "filter": {
        "type": "and",
        "fields": [{
          "type": "selector",
          "dimension": "event",
          "value": "currentstatus"
        },
          {
            "type": "selector",
            "dimension": "strvalue",
            "value": "disabled"
          }]
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Disabled Users",
    "isDefault": false,
    "myName": "apps_inactiveusers"
  },
  {
    "aggregation": {
      "aggregator": {
        "fieldName": "DistinctUsers",
        "name": "Active Searchers",
        "type": "hyperUnique"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "searchcount"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    },
    "dataSource": "apps_searchprivacy",
    "displayName": "Active Searchers",
    "isDefault": false,
    "myName": "apps_activesearchers"
  }
]
print len(y)