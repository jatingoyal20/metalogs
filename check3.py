#!/usr/bin/env python
from flask import Flask, request, render_template, session, url_for, redirect, make_response
from pydruid.client import *
from pydruid.utils.aggregators import longsum
from pydruid.utils.filters import *
from collections import defaultdict
query = PyDruid('http://10.5.1.8:8082', 'druid/v2/')

dimensions=[]
meta=query.segment_metadata(
        datasource=  "apps_stats_audit",
        intervals= ["2016-10-01/2016-10-02"]
)
for key,value in meta[0]['columns'].iteritems():
    if(value['cardinality']!= None):
        dimensions.append(key)
dimensions.sort()
#dimensions =dimensions[:3]
print dimensions
