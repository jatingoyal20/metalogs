
#!/usr/bin/env python
import json
import requests
import common

class cumulateGraph:
    def __init__(self,filters,startdate,enddate,url,granularity):
        filters=common.getFilter(filters)
        self.url=url
        self.query=dict()
        self.query["queryType"] = "timeseries"
        self.query["intervals"] = startdate + "/" +enddate
        self.query["granularity"] = granularity
        self.query["descending"] = "false"
        if filters:
            self.query["filter"] = filters


    def queryDruidGraph(self,datasource,metrics):
        self.query["dataSource"] = datasource
        self.query["aggregations"] = []
        for metricName in metrics:
            agg = common.getMetricLookup(metricName)
            self.query["aggregations"].append(agg)

        print json.dumps(self.query)
        r = requests.post(self.url, data=json.dumps(self.query))
        return r.json()


    def batchQuerying(self,metrics):
        maps=dict()
        final=dict()
        ffinal=dict()
        for metric in metrics:
            #print metric,common.getMetricDisplayName(metric)
            final[common.getMetricDisplayName(metric)]={}
            final[common.getMetricDisplayName(metric)]["cumulative"]=[]
            if(common.getMetricDataSource(metric) not in maps):
                maps[common.getMetricDataSource(metric)]=[]
            maps[common.getMetricDataSource(metric)].append(metric)

        for datasource, metricd in maps.iteritems():
            query=self.queryDruidGraph(datasource,metricd)
            final["timestamp"] = []
            if "error" not in query:
                for entity in query:
                    final["timestamp"].append(entity["timestamp"])
                    for metricName,value in entity["result"].iteritems():
                        final[(metricName)]["cumulative"].append(value)

        ffinal["timestamp"]=final["timestamp"]
        for metric in metrics:
            ffinal[metric+','+common.getMetricDisplayName(metric)]=final[common.getMetricDisplayName(metric)]


        #print metrics
        return ffinal