
#!/usr/bin/env python
import json
import requests
import common

class groupByGraph:
    def __init__(self,groupby,filters,startdate,enddate,url,granularity,tables):

        self.tables = tables
        filters=common.getFilter(filters)
        self.groupby=common.getDimensionDisplayName(groupby)

        self.url=url
        self.query=dict()
        self.query["queryType"] = "topN"
        self.query["granularity"] = granularity
        self.query["intervals"] = startdate + "/" + enddate
        if filters:
            self.query["filter"] = filters
        self.query["dimension"] = common.getDimensionLookup(groupby)
        self.query["descending"] = "false"
        self.query["threshold"]=100



    def queryDruidGraph(self,datasource,metrics):
        self.query["dataSource"] = datasource
        self.query["aggregations"] = []
        self.query["metric"] = common.getMetricDisplayName(metrics[0]) #default metric to sort on

        for metricName in metrics:
            self.query["aggregations"].append(common.getMetricLookup(metricName))

      #  print json.dumps(self.query)
        r = requests.post(self.url, data=json.dumps(self.query))
        return r.json()


    def batchQuerying(self,metrics):
        final=dict()
        ffinal=dict()

        maps=dict()
        for metricName in metrics:
            final[common.getMetricDisplayName(metricName)] = dict()
            if(common.getMetricDataSource(metricName) not in maps):
                maps[common.getMetricDataSource(metricName)]=[]
            maps[common.getMetricDataSource(metricName)].append(metricName)
            for table in self.tables:
                final[common.getMetricDisplayName(metricName)][table] = []

        for datasource, metricd in maps.iteritems():
            queries=self.queryDruidGraph(datasource,metricd)
            final["timestamp"] = []

            if "error" not in queries:
                for query in queries:
                    final["timestamp"].append(query["timestamp"])
                    for metricName in metricd:
                        for table in self.tables:
                            display=common.getMetricDisplayName(metricName)
                            final[display][table].append(0)

                    for element in query["result"]:
                        for metricName in metricd:
                            display=common.getMetricDisplayName(metricName)
                            id=element[self.groupby]
                            final[display][id][-1]=element[display]


        ffinal["timestamp"]=final["timestamp"]
        for metric in metrics:
            ffinal[metric+','+common.getMetricDisplayName(metric)]=final[common.getMetricDisplayName(metric)]


        #print metrics
        return ffinal