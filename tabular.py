#!/usr/bin/env python
import json
import requests
import common

class Tabular:

    def __init__(self,limit,filters,startdate,enddate,url):

        self.limit=limit
        self.startdate = startdate
        self.enddate = enddate
        self.url = url
        self.filters=common.getFilter(filters)

    def addregex(self,dimension,regex):

        pattern = {"type": "regex", "dimension": common.getDimensionDruidName(dimension), "pattern": regex}
        lookup = common.getDimensionLookup(dimension)
        if "name" in lookup:
            pattern["extractionFn"] = {"type": "registeredLookup", "lookup": lookup["name"],
                                        "replaceMissingValueWith": "Unknown"}

        if self.filters:
            self.filters["fields"].append(pattern)
        else:
            self.filters = pattern


    def queryDruidTable(self,metricName,fieldName):

        query=dict()
        query["queryType"] = "topN"
        query["dataSource"] = common.getMetricDataSource(metricName)
        query["intervals"]=self.startdate+"/"+self.enddate
        query["granularity"] = "all"
        if self.filters:
            query["filter"]=self.filters
        query["aggregations"] = [common.getMetricLookup(metricName)]
        query["dimension"]=common.getDimensionLookup(fieldName)
        query["metric"]=common.getMetricDisplayName(metricName)
        query["threshold"]=self.limit
        r = requests.post(self.url, data=json.dumps(query))
        result=r.json()

        if "error" in result:
            del result["error"]

        if result:
            result[0]["dimension"]=common.getDimensionDisplayName(fieldName)
            result[0]["metric"]=common.getMetricDisplayName(metricName)

        return result

    def batchQuerying(self,dimensions):
        html = dict(tuple)

        for dimension in dimensions:
            html[dimension] = []
            responses = self.queryDruidTable(dimension)
            if responses:
                responses=responses[0]["result"]
            for value in responses:
                html[dimension].append((value[dimension], value["count"]))
        return html