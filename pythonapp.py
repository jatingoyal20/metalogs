#!/usr/bin/env python
from pydruid.client import *
from pydruid.utils.aggregators import longsum
from pydruid.utils.filters import *
from collections import defaultdict
import datetime
import requests
from groupByStandard import *
from groupByCohort import *

import common
from tabular import *

now=Tabular(10,{},"2016-10-01T00:00","2016-10-01T23:59",'http://10.5.1.8:8082/druid/v2/')
print now.queryDruidTable("apps_eventvalue","apps_aff_id")

dimension=dict()
with open('config/dimensions.json') as json_data:
    dimensionList = json.load(json_data)
    for value in dimensionList:
        if " " in value["druidName"] :
            print value["druidName"]
        dimension[value["myName"]]=value


print "DOne"


r = requests.get("http://10.5.1.8:8082/druid/v2/datasources/apps_searchprivacy/dimensions")
print r.json()
map=dict()

map["ih"]=[]
map["ih"].append("SD")
filter=["DS","SD"]
query=dict()
query["SD"]=list(filter)
query["SD"].append("F")
print filter

for key,values in map.iteritems():
    for value in values:
        print key,value

st=[{"hi":2},{"hi":3},{"hi":1}]
sorted(st, key=lambda student: student["hi"])
print  st

query = PyDruid('http://10.5.1.8:8082', 'druid/v2/')
true=True
top_langs = query.topn(
    datasource="apps_stats_audit",
    granularity="all",
    intervals="2016-08-01T00:00/2016-08-02T00",
    dimension=  {
      "type" : "extraction",
      "dimension" : "aff_id",
      "outputName": "Affiliate ID",
      "extractionFn" : {
        "type" : "lookup",
        "lookup" : {
          "type" : "namespace",
          "namespace" : "aff_id"
        },
        "retainMissingValue" : true,
        "injective" : true
      }
    },
    filter = Filter(dimension="aff_id",pattern=(".*"),type="regex"),
    aggregations={"coutns": {"type": "count", "fieldName": "count","name":"coutns"}},
    metric="coutns",
    threshold=10
)
ok={
      "aggregator": {
        "fieldName": "event_value",
        "name": "Installs",
        "type": "longSum"
      },
      "filter": {
        "fields" :[
          {
            "dimension": "event",
            "type": "selector",
            "value": "InstallSuccess"
          }
        ],
        "type" : "and"
      },
      "type": "filtered"
    }
ts = query.timeseries(
    datasource='apps_searchprivacy',
    granularity='day',
    intervals='2018-10-02/2018-10-03',
    aggregations={'Installs': ok}
)
ja=dict()

ja["Sd"]={"SD":"SD"}

print json.dumps(ja)
print datetime.datetime.now().isoformat().split('T')[0]
print query.export_pandas().to_json()
#lis = list(df.columns.values)[0]
print os